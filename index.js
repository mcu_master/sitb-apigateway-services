var express = require('express');
var cors = require('cors');

var app = express();
app.set('port', 9003);
app.use(cors());

var mysql = require('mysql');
var mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'twitter_db',
});

mysqlConnection.connect(function (err) {
    if (err) {
        console.error('MySQL error connecting: ' + err.stack);
        return;
    }
    console.log('MySQL is Connected');
});

app.get('/', function (request, response) {
    response.send('API GW is running');
}).listen(app.get('port'), function () {
    console.log('App API GW is running, server is listening on port ', app.get('port'));
});

app.get('/tweet-location', function (req, res) {
    let limit = req.query.limit;
    if (typeof limit === 'undefined') {
        limit = ``;
    } else {
        limit = `limit 0,${limit}`;
    }

    mysqlConnection.query(
        `SELECT place, COUNT(*) AS total FROM tweet_copy WHERE place IS NOT NULL GROUP BY place ORDER BY total DESC ${limit}`,
        function (err, result) {
            if (err) throw err;
            res.send({
                result,
            });
        }
    );
});

app.get('/tweets', function (req, res) {
    let sentiment = req.query.sentiment;
    let place = req.query.place;
    let limit = req.query.limit;

    if (typeof sentiment === 'undefined') sentiment = '';
    if (typeof place === 'undefined') place = '';
    if (typeof limit === 'undefined') {
        limit = ``;
    } else {
        limit = `limit 0,${limit}`;
    }
    mysqlConnection.query(
        `select * from tweet_copy where sentiment like '%${sentiment}%' and place like '%${place}%' order by created_at desc ${limit}`,
        function (err, result) {
            if (err) throw err;
            res.send({
                result,
            });
        }
    );
});

app.get('/sentiment-group', function (req, res) {
    mysqlConnection.query(
        `SELECT sentiment, COUNT(*) AS total FROM tweet_copy GROUP BY sentiment`,
        function (err, result) {
            if (err) throw err;
            res.send({
                result,
            });
        }
    );
});


// TODO Number of tweet
app.get('/totalTweets', function (req, res) {
    let sql = `select count(*) as total from tweets where places != null`;
    mysqlConnection.query(
        sql,
        function (err, result) {
            if (err) throw err;
            res.send({
                result,
            })
        }
    );
});

// TODO number of user
app.get('/user/total', function (req, res) {
    let sql = `select distinct(*) as total_user from tweets where places != null`;
    mysqlConnection.query(
        sql,
        function (err, result) {
            if (err) throw err;
            res.send({
                result,
            })
        }
    );
});

// TODO total reach
// TODO top location
// TODO top user
// TODO active user
// TODO popular tweets
// TODO latest tweets